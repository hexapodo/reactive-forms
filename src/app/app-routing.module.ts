import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasicFormComponent } from './components/basic-form/basic-form.component';
import { DynamicValidationsFormComponent } from './components/dynamic-validations-form/dynamic-validations-form.component';
import { CustomValidationsFormComponent } from './components/custom-validations-form/custom-validations-form.component';
import { ComplexStructureFormComponent } from './components/complex-structure-form/complex-structure-form.component';
import { ConditionalValidationsFormComponent } from './components/conditional-validations-form/conditional-validations-form.component';

const routes: Routes = [
  { path: 'basic', component: BasicFormComponent },
  { path: 'dynamicValidations', component: DynamicValidationsFormComponent },
  { path: 'customValidations', component: CustomValidationsFormComponent },
  { path: 'complexStructure', component: ComplexStructureFormComponent },
  { path: 'conditionalValidations', component: ConditionalValidationsFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
