import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BasicFormComponent } from './components/basic-form/basic-form.component';
import { DynamicValidationsFormComponent } from './components/dynamic-validations-form/dynamic-validations-form.component';
import { CustomValidationsFormComponent } from './components/custom-validations-form/custom-validations-form.component';
import { ComplexStructureFormComponent } from './components/complex-structure-form/complex-structure-form.component';
import { ConditionalValidationsFormComponent } from './components/conditional-validations-form/conditional-validations-form.component';

@NgModule({
  declarations: [
    AppComponent,
    BasicFormComponent,
    DynamicValidationsFormComponent,
    CustomValidationsFormComponent,
    ComplexStructureFormComponent,
    ConditionalValidationsFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
