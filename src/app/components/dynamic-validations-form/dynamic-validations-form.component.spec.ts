import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicValidationsFormComponent } from './dynamic-validations-form.component';

describe('DynamicValidationsFormComponent', () => {
  let component: DynamicValidationsFormComponent;
  let fixture: ComponentFixture<DynamicValidationsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicValidationsFormComponent]
    });
    fixture = TestBed.createComponent(DynamicValidationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
