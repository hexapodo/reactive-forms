import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dynamic-validations-form',
  templateUrl: './dynamic-validations-form.component.html',
  styleUrls: ['./dynamic-validations-form.component.scss']
})
export class DynamicValidationsFormComponent implements OnInit {
  form: FormGroup = new FormGroup({})

  constructor(
    private fb: FormBuilder
  ){}

  ngOnInit(): void {
    this.form = this.fb.group({
      nombre: [''],
      edad: [],
    });
  }

  enviarForm() {
    console.log('Se va a enviar el form con los datos:', this.form.value)
  }

  addValidations() {
    this.form.get('nombre')?.addValidators(Validators.required);
    this.form.get('edad')?.addValidators([Validators.required, Validators.min(0)]);
    this.form.get('nombre')?.updateValueAndValidity();
    this.form.get('edad')?.updateValueAndValidity();
  }

  removeValidations() {
    this.form.get('nombre')?.clearValidators();
    this.form.get('edad')?.clearValidators();
    this.form.get('nombre')?.updateValueAndValidity();
    this.form.get('edad')?.updateValueAndValidity();
  }

}
