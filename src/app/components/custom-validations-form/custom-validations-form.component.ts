import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-custom-validations-form',
  templateUrl: './custom-validations-form.component.html',
  styleUrls: ['./custom-validations-form.component.scss']
})
export class CustomValidationsFormComponent implements OnInit {
  form: FormGroup = new FormGroup({})

  constructor(
    private fb: FormBuilder
  ){}

  ngOnInit(): void {
    this.form = this.fb.group({
      anotacion: ['La bebida gaseosa que mas me gusta es: cocacola', this.palabrasProhibidasValidator],
    });
  }

  enviarForm() {
    console.log('Se va a enviar el form con los datos:', this.form.value)
  }

  palabrasProhibidasValidator(field: AbstractControl): null | { customError: boolean } {
    const palabraProhibida = 'pepsi'; 
    if (field.value.includes(palabraProhibida)) {
      return { customError: true };
    }
    return null;
  }
}
