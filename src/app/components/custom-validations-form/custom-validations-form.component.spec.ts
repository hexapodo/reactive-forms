import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomValidationsFormComponent } from './custom-validations-form.component';

describe('CustomValidationsFormComponent', () => {
  let component: CustomValidationsFormComponent;
  let fixture: ComponentFixture<CustomValidationsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomValidationsFormComponent]
    });
    fixture = TestBed.createComponent(CustomValidationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
