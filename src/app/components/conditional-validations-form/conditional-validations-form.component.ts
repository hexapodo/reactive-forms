import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-conditional-validations-form',
  templateUrl: './conditional-validations-form.component.html',
  styleUrls: ['./conditional-validations-form.component.scss']
})
export class ConditionalValidationsFormComponent implements OnInit {

  form: FormGroup = new FormGroup({})
  trabajadorForm: FormGroup = new FormGroup({})
  estudianteForm: FormGroup = new FormGroup({})

  constructor(
    private fb: FormBuilder
  ){}

  ngOnInit(): void {
    this.form = this.fb.group({
      nombre: ['', [Validators.required]],
      tipo: ['estudiante', Validators.required],
      referencias: this.fb.array([], [Validators.minLength(2), Validators.required])
    });

    this.trabajadorForm = this.fb.group({
      empresa: ['', Validators.required],
      cargo: ['', Validators.required],
    });

    this.estudianteForm = this.fb.group({
      institucion: ['', Validators.required],
      programa: ['', Validators.required],
      semestre: ['', [Validators.min(1), Validators.max(12)]],
    });

    this.form.addControl('estudiante', this.estudianteForm);

    this.form.get('tipo')?.valueChanges.subscribe({
      next: value => {
        if (value === 'estudiante') {
          this.form.removeControl('trabajador');
          this.form.addControl('estudiante', this.estudianteForm);
        } else {
          this.form.removeControl('estudiante');
          this.form.addControl('trabajador', this.trabajadorForm);
        }
      }
    });
  }

  fillTheWholeForm() {
    const userData = {
      nombre: "Pepito Pérez Oso.",
      tipo: "estudiante",
      estudiante: {
        institucion: "Simón Bolivar",
        programa: "Ingeniería culinaria",
        semestre: 4
      },
      trabajador: {
        empresa: "Globant",
        cargo: "UI Dev"
      },
      referencias: [
        {
          nombre: "Claudia",
          celular: "3123123123",
          afinidad: "esposa"
        },
        {
          nombre: "Natalia",
          celular: "34534534545",
          afinidad: ""
        }
      ]
    };

    this.form.patchValue(userData);
  }

  addReferencia() {
    const referencia = this.fb.group({
      nombre: ['', Validators.required],
      celular: ['', Validators.required],
      afinidad: ['']
    });
    (this.form.get('referencias') as FormArray).push(referencia);
  }

  removeReferencia(i: number) {
    (this.form.get('referencias') as FormArray).removeAt(i);
  }

  get referencias() {
    return this.form.get('referencias') as FormArray
  }


  enviarForm() {
    console.log('Se va a enviar el form con los datos:', this.form.value)
  }
}
