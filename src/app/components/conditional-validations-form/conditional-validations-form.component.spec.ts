import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionalValidationsFormComponent } from './conditional-validations-form.component';

describe('ConditionalValidationsFormComponent', () => {
  let component: ConditionalValidationsFormComponent;
  let fixture: ComponentFixture<ConditionalValidationsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConditionalValidationsFormComponent]
    });
    fixture = TestBed.createComponent(ConditionalValidationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
