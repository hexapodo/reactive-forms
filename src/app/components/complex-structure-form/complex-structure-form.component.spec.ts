import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplexStructureFormComponent } from './complex-structure-form.component';

describe('ComplexStructureFormComponent', () => {
  let component: ComplexStructureFormComponent;
  let fixture: ComponentFixture<ComplexStructureFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ComplexStructureFormComponent]
    });
    fixture = TestBed.createComponent(ComplexStructureFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
