import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-complex-structure-form',
  templateUrl: './complex-structure-form.component.html',
  styleUrls: ['./complex-structure-form.component.scss']
})
export class ComplexStructureFormComponent  implements OnInit{
  form: FormGroup = new FormGroup({})

  constructor(
    private fb: FormBuilder
  ){}

  ngOnInit(): void {
    this.form = this.fb.group({
      nombre: ['', [Validators.required]],
      datosContacto : this.fb.group({
        celular: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        direccion : ['']
      }),
      datosOficina: this.fb.group({
        antiguedadMeses: [0, [Validators.required, Validators.min(1)]],
        dependencia: this.fb.group({
          nombre: ['', [Validators.required]],
          cargo: this.fb.group({
            nombre: ['', [Validators.required]],
            esJefe: [false]
          })
        })
      }),
      experiencia: this.fb.array([
        this.fb.group({
          empresa: ['', Validators.required],
          cargo: ['', Validators.required],
          tiempoMeses: [0, [Validators.required, , Validators.min(1)]],
        }),
        this.fb.group({
          empresa: ['', Validators.required],
          cargo: ['', Validators.required],
          tiempoMeses: [0, [Validators.required, , Validators.min(1)]],
        })
      ])
    });
  }

  fillTheWholeForm() {
    const userData = {
      nombre: "Pepito Pérez Oso",
      datosContacto: {
        celular: "310 345 7856",
        email: "pepito@jajaja.com",
        direccion: "Cr 16b 60 an 21, Popayán, Cauca"
      },
      datosOficina: {
        antiguedadMeses: "12",
        dependencia: {
          nombre: "Financiera",
          cargo: {
            nombre: "Jefe Financiero",
            esJefe: true
          }
        }
      },
      experiencia: [
        {
          empresa: "Planet Express",
          cargo: "Deliver man",
          tiempoMeses: 12
        },
        {
          empresa: "Pizza Planet",
          cargo: "Deliver coordinator",
          tiempoMeses: 6
        }
      ]
    };

    this.form.patchValue(userData);
  }


  enviarForm() {
    console.log('Se va a enviar el form con los datos:', this.form.value)
  }
}
