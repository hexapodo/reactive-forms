import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs';

@Component({
  selector: 'app-basic-form',
  templateUrl: './basic-form.component.html',
  styleUrls: ['./basic-form.component.scss']
})
export class BasicFormComponent implements OnInit{

  form: FormGroup = new FormGroup({})

  constructor(
    private fb: FormBuilder
  ){}

  ngOnInit(): void {
    this.form = this.fb.group({
      codigo: ['123456', [Validators.required, Validators.minLength(5)]],
      nombre: ['', [Validators.required]],
      email: ['asd@asd.com', [Validators.required, Validators.email]],
      anotacion: ['']
    });
    this.form.get('nombre')?.valueChanges.pipe(debounceTime(1000)).subscribe(console.log);
  }

  fillTheWholeForm() {
    this.form.patchValue({
      nombre: 'Pepito Pérez Oso',
      codigo: 6666666,
      email: "pepito.perez@jaja.com",
      anotacion: "N/A"
    });
  }

  fillEachField() {
    this.form.get('codigo')?.setValue(11111111);
    this.form.get('nombre')?.setValue('Homer Simpson');
    this.form.get('email')?.setValue('homer.simpson@jaja.com');
    this.form.get('anotacion')?.setValue('alguna anotacion: bla bla bla...');
  }

  disableNombre() {
    this.form.get('nombre')?.disabled ? this.form.get('nombre')?.enable() : this.form.get('nombre')?.disable();
  }

  enviarForm() {
    console.log('Se va a enviar el form con los datos:', this.form.value)
  }

}
